 #!/bin/bash
# File Name : post_proc_ACCESS_monthly.sh
# Creation Date : 02/10/2014
# Last Modified : Wed 03 Dec 2014 09:03:31 AEDT
# Created By : Ruth Lorenz
# Purpose : calculate monthly and seasonal means
#		for ACCESS run comparisons
#               e.g. data@ /srv/ccrc/data36/z3381484/ACCESS-Leuning-Medlyn-out/
#                       LEUNING/NC?
#                       MEDLYN/NC?
#           or data@ /srv/ccrc/data37/z3500361/ACCESS1.4/UM_ROUTDIR/mxs561/
# usage : nohup ./post_proc_ACCESS_monthly.sh > outfile.out &
###-------------------------------------------------------

##-----------------------
# function to determine if year is leapyear (1) or not (0)
##-----------------------

function leapyr ()
{ 
  if [[  ${year}%4 -eq "0" && ${year}%100 -ne "0" || ${year}%400 -eq "0" ]]; then
    local __lpyr=1
  else
    local __lpyr=0
  fi
  echo "$__lpyr"
}

##-----------------------##
## load required modules ##
##-----------------------##

module load netcdf
module load nco
module load cdo
 
##-----------------------##
## user specifications   ##
## adjust to your needs! ##
##-------------------- --##
#exp: RUNID given by umuix
exp=valip
#start and end year to be processed
#Attention: needs data from december the year before for correct seasonal means!
year_start=1957
year_end=2005
#directory the data is archived in
archive=/srv/ccrc/data37/z3500361/ACCESS1.4/UM_ROUTDIR/mxs561/$exp/
#working directory for processing data
workdir=/srv/ccrc/data44/z3441306/ACCESS_LAI_MA/work1/
#output directory
outdir=/srv/ccrc/data44/z3441306/ACCESS_LAI_MA/test/

#timeperiod of the data to be processed, e.g. monthly, daily
timeperiod=monthly

#if only certain variables are to be included in the postprocessed files, specify here, otherwise leave "Flags" empty
variables=pr,tas,rss,rls,hfls,hfss,alb,cancd,npp
Flags="-v ${variables}"

#infos for global attributes in netcdf
TITLE_RUN="ACCESS1.4 output from 5d3 "
SOURCE="ACCESS1.4, AMIP, N96\n"
INSTITUTION="ARC Centre of Excellence for Climate System Science,\n model run at NCI National Facility, Australia\n"
CONTACT="shaoxiu.ma@unsw.edu.au"

##---------------------##

mkdir -p ${outdir}
mkdir -p ${workdir}

cd ${workdir}

if [[ "$timeperiod" == "monthly" ]]; then
##-------------------------------------------##
## monthly files:                            ##
##-------------------------------------------##

## get data from archive, loop over year, unzip
## and concatenate yearly archives          

year=$year_start

     while [  $year -le $year_end ]; do
       #loop over years
       echo "Processing year $year"

            #for first year get december of year before (for seasonal means)	
		  if [ $year -eq $year_start ]; then
		      let "year_before=year-1"
		      ncrcat -O ${Flags} ${archive}/$exp.monthly.$year_before-12-00T00.nc  \
			  ${archive}/$exp.monthly.$year-??-00T00.nc ${workdir}/$exp.monthly.dec$year_before-$year.nc
		      ncrcat -O ${Flags} ${archive}/$exp.monthly.$year-??-00T00.nc ${workdir}/$exp.monthly.$year.nc
		  elif [ $year -eq $year_end ]; then
		      ncrcat -O ${Flags} ${archive}/$exp.monthly.$year-??-00T00.nc ${workdir}/$exp.monthly.$year.nc
		      ncrcat -O ${Flags} ${archive}/$exp.monthly.$year-0?-00T00.nc \
			   ${archive}/$exp.monthly.$year-10-00T00.nc ${archive}/$exp.monthly.$year-11-00T00.nc ${workdir}/$exp.monthly.$year-dec.nc
		  else 
		      ncrcat -O ${Flags} ${archive}/$exp.monthly.$year-??-00T00.nc ${workdir}/$exp.monthly.$year.nc
		  fi

		let year=year+1
	done #year

#Create timeseries over whole time frame
# monthly mean climatology and seasonal means for monthly files

echo "Create seasonal averages and monthly timeseries..."

#create timeseries and monthly means
ncrcat -O $exp.monthly.????.nc $exp.monthly_TS.${year_start}_${year_end}.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" $exp.monthly_TS.${year_start}_${year_end}.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" $exp.monthly_TS.${year_start}_${year_end}.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" $exp.monthly_TS.${year_start}_${year_end}.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" $exp.monthly_TS.${year_start}_${year_end}.nc
ncatted -h -O -a  comment,global,a,c,"Monthly timeseries" $exp.monthly_TS.${year_start}_${year_end}.nc

#create seasonal averages
rm $exp.monthly.$year_end.nc
rm $exp.monthly.$year_start.nc
ncrcat -O $exp.monthly.dec$year_before-$year_start.nc $exp.monthly.????.nc $exp.monthly.$year_end-dec.nc \
    $exp.monthly_TSseas.${year_start}_${year_end}.nc

cdo yseasmean $exp.monthly_TSseas.${year_start}_${year_end}.nc $exp.seasonal_means.${year_start}_${year_end}.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" $exp.seasonal_means.${year_start}_${year_end}.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" $exp.seasonal_means.${year_start}_${year_end}.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" $exp.seasonal_means.${year_start}_${year_end}.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" $exp.seasonal_means.${year_start}_${year_end}.nc
ncatted -h -O -a  comment,global,a,c,"Seasonal means over all years" $exp.seasonal_means.${year_start}_${year_end}.nc

## zip and move monthly timeseries to output
#gzip $exp.monthly_TS.${year_start}_${year_end}.nc
#gzip $exp.seasonal_means.${year_start}_${year_end}.nc

# Move output data to OUTDIR space.
echo "The monthly output files are now moved to ${outdir}."

mv $exp.monthly_TS.${year_start}_${year_end}.nc ${outdir}
mv $exp.seasonal_means.${year_start}_${year_end}.nc ${outdir}

#clean up
rm ${workdir}/*

fi
##-------------------------------------------##
## calculate Tmax and Tmin seasonal means    ##
##-------------------------------------------##
timeperiod=daily
  year=$year_start

     while [  $year -le $year_end ]; do
       #loop over years
       echo "Processing year $year for timeperiod $timeperiod"

		  ncrcat -O ${archive}/$exp.$timeperiod.$year-??-00T00.nc $exp.$timeperiod.$year.nc

		  #if daily, extract tmax and tmin for monthly means
		  if [[ "$timeperiod" == "daily" ]]; then
		      ncks -O -v tasmax,tasmin $exp.$timeperiod.$year.nc $exp.${timeperiod}_tmax_tmin.$year.nc
		      
		      #take december from year before but not last december for seasonal means
		      if [ $year -eq $year_start ]; then
		      let year_before=year-1
		      ncks -O -v tasmax,tasmin ${archive}/$exp.$timeperiod.${year_before}-12-00T00.nc $exp.${timeperiod}_tmax_tmin.dec$year_before.nc
		      ncrcat -O $exp.${timeperiod}_tmax_tmin.dec$year_before.nc $exp.${timeperiod}_tmax_tmin.$year.nc $exp.${timeperiod}_tmax_tmin.dec${year_before}_$year.nc
		      cdo monmean $exp.${timeperiod}_tmax_tmin.dec${year_before}_$year.nc $exp.monmean_tmax_tmin.dec${year_before}_$year.nc
		      elif [ $year -eq $year_end ]; then
			  let endT=333+$(leapyr)
			  echo "$year is leapyear?: $(leapyr)"
		      ncks -O -d time,0,$endT $exp.${timeperiod}_tmax_tmin.$year_end.nc $exp.${timeperiod}_tmax_tmin.$year_end-dec.nc
		      cdo monmean $exp.${timeperiod}_tmax_tmin.$year_end-dec.nc $exp.monmean_tmax_tmin.$year_end-dec.nc
		      fi
		      cdo monmean $exp.${timeperiod}_tmax_tmin.$year.nc $exp.monmean_tmax_tmin.$year.nc
		      rm $exp.$timeperiod.$year.nc
		  fi
		  let year=year+1
       done #year
  # Create timeseries
  echo "Create $timeperiod timeseries..."

  ncrcat -O $exp.monmean_tmax_tmin.????.nc $exp.monmean_tmax_tmin.${year_start}_${year_end}.nc
  #add global attributes
  ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" $exp.monmean_tmax_tmin.${year_start}_${year_end}.nc
  ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" $exp.monmean_tmax_tmin.${year_start}_${year_end}.nc
  ncatted -h -O -a source,global,a,c,"${SOURCE}" $exp.monmean_tmax_tmin.${year_start}_${year_end}.nc
  ncatted -h -O -a  contact,global,a,c,"${CONTACT}" $exp.monmean_tmax_tmin.${year_start}_${year_end}.nc

  rm $exp.monmean_tmax_tmin.$year_start.nc
  rm $exp.monmean_tmax_tmin.$year_end.nc
  ncrcat -O $exp.monmean_tmax_tmin.dec${year_before}_????.nc $exp.monmean_tmax_tmin.????.nc $exp.monmean_tmax_tmin.$year_end-dec.nc $exp.monmean_tmax_tmin_forseas.${year_start}_${year_end}.nc
  cdo yseasmean $exp.monmean_tmax_tmin_forseas.${year_start}_${year_end}.nc $exp.seasmean_tmax_tmin.${year_start}_${year_end}.nc
  #add global attributes
  ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" $exp.seasmean_tmax_tmin.${year_start}_${year_end}.nc
  ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" $exp.seasmean_tmax_tmin.${year_start}_${year_end}.nc
  ncatted -h -O -a source,global,a,c,"${SOURCE}" $exp.seasmean_tmax_tmin.${year_start}_${year_end}.nc
  ncatted -h -O -a  contact,global,a,c,"${CONTACT}" $exp.seasmean_tmax_tmin.${year_start}_${year_end}.nc

  #compress files
  #gzip $exp.seasmean_tmax_tmin.${year_start}_${year_end}.nc
  #gzip $exp.monmean_tmax_tmin.${year_start}_${year_end}.nc

  # Move output data to output DIR.
  echo "Move output to ${outdir}."
  mv $exp.monmean_tmax_tmin.${year_start}_${year_end}.nc ${outdir}
  mv $exp.seasmean_tmax_tmin.${year_start}_${year_end}.nc ${outdir}

#clean up
rm ${workdir}/*

#END

