#!/bin/bash
# File Name : post_proc_ACCESS_prep_climdex.sh
# Creation Date : 26/09/2014
# Last Modified : Fri 26 Sep 2014 08:19:34 EST
# Created By : Ruth Lorenz
# Purpose : prepare ACCESS runs for
#		climdex analysis with fclimdex code
#		e.g. data@ /srv/ccrc/data36/z3381484/ACCESS-Leuning-Medlyn-out/
#			LEUNING/NC?
#			MEDLYN/NC?

###-------------------------------------------------------
module load netcdf
module load nco
module load cdo

##-----------------------##
## user specifications   ##
## adjust to your needs! ##
##---------------------- ##
#exp: RUNID given by umuix
exp=vafqd
#start and end year to be processed
year_start=2013
year_end=2099
#directory the data is archived in
archive=/srv/ccrc/data36/z3381484/ACCESS-Leuning-Medlyn-out-RCP85/MEDLYN/NC4/
#working directory for processing data
workdir=/srv/ccrc/data44/z3441306/ACCESS-Leuning-Medlyn-RCP85/MEDLYN/NC4/work/
#output directory
outdir=/srv/ccrc/data44/z3441306/ACCESS-Leuning-Medlyn-RCP85/MEDLYN/test/

timeperiod=daily

#if only certain variables are to be included in the postprocessed files,
#specify here, otherwise leave "Flags" empty
variables=pr,tas,tasmin,tasmax,rss,rls,hfls,hfss
Flags="-v ${variables}"

#infos for global attributes in netcdf
TITLE_RUN="ACCESS1.3b output with new Medlyn equation, ensemble member 4"
SOURCE="ACCESS1.3b (HadGEM3-CABLE2.0.1), AMIP, N96, RCP8.5\n"
INSTITUTION="ARC Centre of Excellence for Climate System Science,\n model run at NCI National Facility, Australia\n"
CONTACT="j.kala@unsw.edu.au"

##---------------------##

mkdir -p ${outdir}
mkdir -p ${workdir}

cd ${workdir}

##-------------------------------------------##
## daily files:                              ##
##-------------------------------------------##


  year=$year_start

     while [  $year -le $year_end ]; do
       #loop over years
       echo "Processing year $year for timeperiod $timeperiod"

		  ncrcat -O ${Flags} ${archive}/$exp.$timeperiod.$year-??-00T00.nc ${workdir}/$exp.$timeperiod.$year.nc

		  let year=year+1
     done #year

  # Create timeseries
  echo "Create $timeperiod timeseries..."
  ncrcat -O $exp.${timeperiod}.????.nc $exp.${timeperiod}_TS.${year_start}_${year_end}.nc
  #add global attributes
  ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" $exp.${timeperiod}_TS.${year_start}_${year_end}.nc
  ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" $exp.${timeperiod}_TS.${year_start}_${year_end}.nc
  ncatted -h -O -a source,global,a,c,"${SOURCE}" $exp.${timeperiod}_TS.${year_start}_${year_end}.nc
  ncatted -h -O -a  contact,global,a,c,"${CONTACT}" $exp.${timeperiod}_TS.${year_start}_${year_end}.nc

  #clean up 
  rm $exp.$timeperiod.????.nc

  #prepare daily Tmax,Tmin and PR for climdex
  #change reference time to 1950 to avoid problems with negative times
  #remove missing_value and _FillValue attributes which cause problems in fortran code, no missing values are present in model output
      ncks -O -v tasmin ${exp}.daily_TS.${year_start}_${year_end}.nc ${exp}_TN_${year_start}-${year_end}.nc
      cdo setreftime,1950-01-01,00:00:00,days ${exp}_TN_${year_start}-${year_end}.nc ${exp}_TN_${year_start}-${year_end}_ref1950.nc
      ncatted -a missing_value,Tmin,d,, ${exp}_TN_${year_start}-${year_end}_ref1950.nc
      ncatted -a _FillValue,Tmin,d,, ${exp}_TN_${year_start}-${year_end}_ref1950.nc
      
      ncks -O -v tasmax ${exp}.daily_TS.${year_start}_${year_end}.nc ${exp}_TX_${year_start}-${year_end}.nc
      cdo setreftime,1950-01-01,00:00:00,days ${exp}_TX_${year_start}-${year_end}.nc ${exp}_TX_${year_start}-${year_end}_ref1950.nc
      ncatted -a missing_value,Tmax,d,, ${exp}_TX_${year_start}-${year_end}_ref1950.nc
      ncatted -a _FillValue,Tmax,d,, ${exp}_TX_${year_start}-${year_end}_ref1950.nc

      ncks -O -v pr ${exp}.daily_TS.${year_start}_${year_end}.nc ${exp}_PR_${year_start}-${year_end}.nc
      cdo setreftime,1950-01-01,00:00:00,days ${exp}_PR_${year_start}-${year_end}.nc ${exp}_PR_${year_start}-${year_end}_ref1950.nc
      ncatted -a missing_value,prcp,d,, ${exp}_PR_${year_start}-${year_end}_ref1950.nc
      ncatted -a _FillValue,prcp,d,, ${exp}_PR_${year_start}-${year_end}_ref1950.nc

 #compress file with all variables
  gzip $exp.${timeperiod}_TS.${year_start}_${year_end}.nc

  # Move output data to output DIR.
  echo "The $timeperiod output files are now moved to ${outdir}."
  mv ${exp}_TN_${year_start}-${year_end}_ref1950.nc ${outdir}
  mv ${exp}_TX_${year_start}-${year_end}_ref1950.nc ${outdir}
  mv ${exp}_PR_${year_start}-${year_end}_ref1950.nc ${outdir}
  mv $exp.${timeperiod}_TS.${year_start}_${year_end}.nc.gz ${outdir}

#clean up
rm ${workdir}/*

#END
