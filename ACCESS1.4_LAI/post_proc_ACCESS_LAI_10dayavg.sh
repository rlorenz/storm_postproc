#!/bin/bash
## \file post_proc_ACCESS_LAI_10dayavg.sh
#  \author Ruth Lorenz (r.lorenz@unsw.edu.au)
#  \brief create 10 day average timeseries for daily data UM output
#   which is archived on storm servers
#  \usage ./post_proc_ACCESS_LAI_10dayavg.sh

##-----------------------
# function to determine if year is leapyear (1) or not (0)
##-----------------------

function leapyr ()
{ 
  if [[  ${year}%4 -eq "0" && ${year}%100 -ne "0" || ${year}%400 -eq "0" ]]; then
    local __lpyr=1
  else
    local __lpyr=0
  fi
  echo "$__lpyr"
}

##-----------------------##
## load required modules ##
##-----------------------##

module load netcdf
module load nco
module load cdo
 
##---------------------##
## user specifications ##
##-------------------- ##

experiment=30d
year_start=1956
year_end=2005

workdir=/srv/ccrc/data44/z3441306/ACCESS_LAI_MA/work/
outdir=/srv/ccrc/data44/z3441306/ACCESS_LAI_MA/$experiment/

timeperiod=daily

#infos for global attributes in netcdf
TITLE_RUN="ACCESS1.4 output from 30d "
SOURCE="ACCESS1.4, AMIP, N96\n"
INSTITUTION="ARC Centre of Excellence for Climate System Science,\n model run at NCI National Facility, Australia\n"
CONTACT="shaoxiu.ma@unsw.edu.au"

##---------------------##
mkdir -p ${outdir}
mkdir -p ${workdir}

cd ${workdir}

##-------------------------------------------##
## daily, 3 hourly, and hourly files:        ##
##-------------------------------------------##

#for timeperiod in daily 3hourly hourly
#for exp in valie valik valip valiu valiz #5d
#for exp in valia valig valil valiq valiv #CTRL
for exp in valib valih valim valir valiw #30d
  do
    archive=/srv/ccrc/data37/z3500361/ACCESS1.4/UM_ROUTDIR/mxs561/$exp/

  year=$year_start
#  if  [ $year_end -eq 2012]; then
#      let year_end=$year_end-1
#  fi

     while [  $year -le $year_end ]; do
       #loop over years
       echo "Processing year $year for timeperiod $timeperiod"
       for month in 01 02 03 04 05 06 07 08 09 10 11 12
                  do ncks -O -v pr,tas,rss,rls,hfls,hfss,alb ${archive}/$exp.$timeperiod.$year-${month}-00T00.nc $exp.$timeperiod.$year-${month}-00T00.nc
       done #month
       ncks -O -d time,0,27  $exp.$timeperiod.$year-02-00T00.nc $exp.$timeperiod.$year-02-00T00.nc

       ncrcat -O $exp.$timeperiod.$year-??-00T00.nc $exp.$timeperiod.$year.nc
       rm $exp.$timeperiod.$year-??-00T00.nc

       cdo ydrunavg,10 $exp.$timeperiod.$year.nc $exp.$timeperiod.$year.10dayavg.all.nc
       ncks -d time,0,,10 $exp.$timeperiod.$year.10dayavg.all.nc $exp.$timeperiod.$year.10dayavg.nc

       let year=year+1
     done #year

  # Create timeseries
  echo "Create $timeperiod timeseries..."

  ncrcat -O $exp.$timeperiod.????.10dayavg.nc $exp.${timeperiod}_10dayavg.${year_start}_${year_end}.nc
  #add global attributes
  ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" $exp.${timeperiod}_10dayavg.${year_start}_${year_end}.nc
  ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" $exp.${timeperiod}_10dayavg.${year_start}_${year_end}.nc
  ncatted -h -O -a source,global,a,c,"${SOURCE}" $exp.${timeperiod}_10dayavg.${year_start}_${year_end}.nc
  ncatted -h -O -a  contact,global,a,c,"${CONTACT}" $exp.${timeperiod}_10dayavg.${year_start}_${year_end}.nc

  #clean up 
  #rm $exp.$timeperiod.????.nc
  # Move output data to output DIR.
  echo "The $exp $timeperiod output files are now moved to ${outdir}."

  cp $exp.${timeperiod}_10dayavg.${year_start}_${year_end}.nc ${outdir}

done #exp

#calculate ensemble mean
ncea ?????.${timeperiod}_10dayavg.${year_start}_${year_end}.nc ${outdir}/ens_mean_10d_1956_2005_10_mean_multiyears_ex.nc

#clean up
rm ${workdir}/*

#END
