#!/bin/bash
# File Name : post_proc_CABLE_timeseries.sh
# Creation Date : 26/09/13
# Last Modified : 
# Created By : Ruth Lorenz
# Purpose : concatenate CABLE offline output into one file

###-------------------------------------------------------
##-----------------------
#function to determine if year is leapyear (1) or not (0)
##-----------------------

function leapyr ()
{
  if [[  ${year}%4 -eq "0" && ${year}%100 -ne "0" || ${year}%400 -eq "0" ]]; then
    local __lpyr=1
  else
    local __lpyr=0
  fi
  echo "$__lpyr"
}

##-----------------------##
## load required modules ##
##-----------------------##

module load netcdf
module load nco
module load cdo

##---------------------##
## user specifications ##
##-------------------- ##

exp=out_gswp
year_start=1986
year_end=1995
archive=/srv/ccrc/data23/z3441306/CABLE_output/$exp/
workdir=/srv/ccrc/data23/z3441306/CABLE_output/$exp/work/
outdir=/srv/ccrc/data23/z3441306/CABLE_output/$exp/postproc/

##---------------------##
mkdir ${outdir}
mkdir ${workdir}

cd ${workdir}

##-------------------------------------------##
## monthly files:                            ##
##-------------------------------------------##

## get data from mdss, loop over year, unzip
## and concatenate yearly archives

year=$year_start

     while [  $year -le $year_end ]; do
       #loop over years
       echo "Processing year $year"

	file=`ls -l ${archive}/${exp}${year}.nc | awk '{print $2}'`
        if [[ "$file" != 1 ]]; then
        	echo "ERROR: missing file ${exp}${year}.nc in ${archive}" 1>&2
        exit 2
        fi

	cp ${archive}/${exp}${year}.nc .

	#seasonal and monthly means
	#take only december from 1styear but not last december for seasonal means
        if [ $year -eq $year_start ]; then
		let startT=334+$(leapyr)
                echo "$year is leapyear?: $(leapyr)"
                ncks -O -d time,$startT, $exp$year_start.nc $exp${year_start}dec.nc
        elif [ $year -eq $year_end ]; then
        	let endT=333+$(leapyr)
                echo "$year is leapyear?: $(leapyr)"
                ncks -O -d time,0,$endT $exp$year_end.nc $exp$year_end-dec.nc
                cdo monmean $exp$year_end-dec.nc $exp.monmean.$year_end-dec.nc
        fi
        cdo monmean $exp$year.nc $exp.monmean.$year.nc

        let year=year+1
	done #year
  
# Create timeseries
echo "Create monthly timeseries..."

ncrcat -O $exp.monmean.????.nc $exp_monmean_TS_${year_start}-${year_end}.nc
rm $exp.monmean.$year_start.nc
rm $exp.monmean.$year_end.nc

echo "Create seasonal means..."
let year_start_seas=$year_start+1
ncrcat -O $exp${year_start}dec.nc $exp.monmean.????.nc $exp.monmean.$year_end-dec.nc $exp.monmean_forseas.${year_start_seas}_${year_end}.nc
cdo yseasmean $exp.monmean_forseas.${year_start_seas}_${year_end}.nc ${exp}_seasmean_${year_start_seas}_${year_end}.nc

echo "Create daily timeseries..."
ncrcat -O ${archive}/${exp}????.nc ${workdir}/${exp}_daily_TS_${year_start}-${year_end}.nc

#daily files for climdex
ncks -O -v Tmax ${exp}_daily_TS_${year_start}-${year_end}.nc ${exp}_TX_${year_start}-${year_end}.nc
ncatted -a missing_value,Tmax,d,, ${exp}_TX_${year_start}-${year_end}.nc
ncatted -a _FillValue,Tmax,d,, ${exp}_TX_${year_start}-${year_end}.nc
cdo subc,273.15 ${exp}_TX_${year_start}-${year_end}.nc ${exp}_TX_${year_start}-${year_end}_degC.nc

ncks -O -v Tmin ${exp}_daily_TS_${year_start}-${year_end}.nc ${exp}_TN_${year_start}-${year_end}.nc
ncatted -a missing_value,Tmin,d,, ${exp}_TN_${year_start}-${year_end}.nc
ncatted -a _FillValue,Tmin,d,, ${exp}_TN_${year_start}-${year_end}.nc
cdo subc,273.15 ${exp}_TN_${year_start}-${year_end}.nc ${exp}_TN_${year_start}-${year_end}_degC.nc

ncks -O -v Rainf ${exp}_daily_TS_${year_start}-${year_end}.nc ${exp}_PR_${year_start}-${year_end}.nc
ncrename -v Rainf,prcp ${exp}_PR_${year_start}-${year_end}.nc
ncatted -a missing_value,prcp,d,, ${exp}_PR_${year_start}-${year_end}.nc
ncatted -a _FillValue,prcp,d,, ${exp}_PR_${year_start}-${year_end}.nc
cdo mulc,86400 ${exp}_PR_${year_start}-${year_end}.nc  ${exp}_PR_${year_start}-${year_end}_mm.nc

#move data to outdir
echo "The output files are now moved to ${outdir}."
mv ${exp}_TN_${year_start}-${year_end}_degC.nc ${outdir}
mv ${exp}_TX_${year_start}-${year_end}_degC.nc ${outdir}
mv ${exp}_PR_${year_start}-${year_end}_mm.nc ${outdir}
mv ${exp}_daily_TS_${year_start}-${year_end}.nc ${outdir}
mv $exp_monmean_TS_${year_start}_${year_end}.nc ${outdir}
mv $exp_seasmean_${year_start}_${year_end}.nc ${outdir}	

#clean up
#rm $workdir/*
