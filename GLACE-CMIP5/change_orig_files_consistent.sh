#!/bin/bash
# File Name : change_orig_file_consistent.sh
# Creation Date : 15/12/2014
# Last Modified : 
# Created By : Ruth Lorenz
# Purpose : modify original GLACE-CMIP5 files downloaded
#		from ETH to be all consistent

###-------------------------------------------------------

module load netcdf
module load cdo
module load nco

##---------------------##
## user specifications ##
##-------------------- ##
var="rsds"
timeperiod="monthly"

#for model in CESM EC-EARTH ECHAM6 IPSL
for model in ECHAM6
do
    #for exp in CTL GC1A85 GC1B85 
    for exp in GC1A85 GC1B85 
    do
        indir=/srv/ccrc/data32/z3441306/GLACE-CMIP5/$model/$exp/
	echo $indir
	cd ${indir}

	if [[ $model == CESM ]]; then
	    if [[ $exp == CTL ]] && [[ "$timeperiod" == "monthly" ]]; then
		if [[ "$var" == "mrso" ]] ; then
		    ncks -O -d time,1200, ${var}_Lmon_CCSM4_historical_r6i1p1_185001-200512.nc ${var}_Lmon_CCSM4_historical_r6i1p1_195001-200512.nc
		    if [[ -f ${var}_Lmon_CCSM4_rcp85_r6i1p1_200501-210012.nc ]]; then
			ncks -O -d time,12, ${var}_Lmon_CCSM4_rcp85_r6i1p1_200501-210012.nc ${var}_Lmon_CCSM4_rcp85_r6i1p1_200601-210012.nc
		    fi
		    ncrcat -O ${var}_Lmon_CCSM4_historical_r6i1p1_195001-200512.nc ${var}_Lmon_CCSM4_rcp85_r6i1p1_200601-210012.nc ${var}_${timeperiod}_CESM_CTL_1_195001-210012.nc
		else
		    ncks -O -d time,1200, ${var}_Amon_CCSM4_historical_r6i1p1_185001-200512.nc ${var}_Amon_CCSM4_historical_r6i1p1_195001-200512.nc
		    if [[ -f ${var}_Amon_CCSM4_rcp85_r6i1p1_200501-210012.nc ]]; then
			ncks -O -d time,12, ${var}_Amon_CCSM4_rcp85_r6i1p1_200501-210012.nc ${var}_Amon_CCSM4_rcp85_r6i1p1_200601-210012.nc
		    fi
		    ncrcat -O ${var}_Amon_CCSM4_historical_r6i1p1_195001-200512.nc ${var}_Amon_CCSM4_rcp85_r6i1p1_200601-210012.nc ${var}_${timeperiod}_CESM_CTL_1_195001-210012.nc
		fi
	    elif [[ $exp != CTL ]] && [[ "$var" == "mrso" ]]; then
	        mv ${var}_int_monthly_CESM_${exp}_1_195001-210012.nc ${var}_${timeperiod}_CESM_${exp}_1_195001-210012.nc
	    elif [[ $exp != CTL ]] && [[ "$timeperiod" == "monthly" ]] && [[ "$var" == "hfss" ]]; then
		cdo monmean ${var}_daily_CESM_${exp}_1_19500101-21001231.nc ${var}_${timeperiod}_CESM_${exp}_1_195001-210012.nc
	    else
		mv ${var}_CESM_${exp}_1_195001-210012.nc ${var}_${timeperiod}_CESM_${exp}_1_195001-210012.nc
	    fi
	fi

	if [[ $model == EC-EARTH ]]; then
	    if [[ $exp == CTL ]]; then
		cdo setreftime,1950-01-01,00:00:00,days ${var}_daily_EC-EARTH_GC1R85_1_195001-210012.nc ${var}_daily_EC-EARTH_${exp}_1_195001-210012.nc
		cdo monmean ${var}_daily_EC-EARTH_${exp}_1_195001-210012.nc ${var}_${timeperiod}_EC-EARTH_${exp}_1_195001-210012.nc
	    elif [[ $exp != CTL ]]; then
		cdo setreftime,1950-01-01,00:00:00,days ${var}_${timeperiod}_EC-EARTH_${exp}_6_195101-210012.nc ${var}_${timeperiod}_EC-EARTH_${exp}_1_195001-210012.nc
	    fi
	    if [[ "${var}" == "hfss" ]]; then
		ncrename -O -v SSHF,hfss ${var}_${timeperiod}_EC-EARTH_${exp}_1_195001-210012.nc
	    elif [[ "${var}" == "psl" ]]; then
		ncrename -O -v MSL,psl ${var}_${timeperiod}_EC-EARTH_${exp}_1_195001-210012.nc
	    fi
	fi

	if [[ $model == ECHAM6 ]]; then
	    if [[ $exp == CTL ]]; then
		#cdo invertlat  ${var}_${timeperiod}_echam6_REF_1_1950-2100.nc ${var}_${timeperiod}_${model}_${exp}_1_195001-210012.nc
		#cdo invertlat ${var}_${timeperiod}_${model}_${exp}_1_195001-210012.nc ${var}_${timeperiod}_${model}_${exp}_1_195001-210012_invertlat.nc
		mv ${var}_${timeperiod}_${model}_${exp}_1_195001-210012_invertlat.nc ${var}_${timeperiod}_${model}_${exp}_1_195001-210012.nc
	    else
		#cdo invertlat ${var}_${timeperiod}_echam6_${exp}_1_1950-2100.nc ${var}_${timeperiod}_${model}_${exp}_1_195001-210012.nc
		#cdo invertlat ${var}_${timeperiod}_${model}_${exp}_1_195001-210012.nc ${var}_${timeperiod}_${model}_${exp}_1_195001-210012_invertlat.nc
		mv ${var}_${timeperiod}_${model}_${exp}_1_195001-210012_invertlat.nc ${var}_${timeperiod}_${model}_${exp}_1_195001-210012.nc
	    fi
	    if [[ $var == rlds ]]; then
		cdo chname,var49,rlds ${var}_${timeperiod}_${model}_${exp}_1_195001-210012.nc ${var}_${timeperiod}_${model}_${exp}_1_195001-210012_tmp.nc
		mv ${var}_${timeperiod}_${model}_${exp}_1_195001-210012_tmp.nc ${var}_${timeperiod}_${model}_${exp}_1_195001-210012.nc
	    fi
	    if [[ $var == rsds ]]; then
		cdo chname,var48,rsds ${var}_${timeperiod}_${model}_${exp}_1_195001-210012.nc ${var}_${timeperiod}_${model}_${exp}_1_195001-210012_tmp.nc
		mv ${var}_${timeperiod}_${model}_${exp}_1_195001-210012_tmp.nc ${var}_${timeperiod}_${model}_${exp}_1_195001-210012.nc
	    fi
	fi

	#if [[ "$model" == "GFDL" ]]; then
	    #fix time ???
	#fi

	if [[ $model == IPSL ]]; then
	    if [[ "$timeperiod" == "monthly" ]]; then
		if [[ "${var}" == "hfls" ]]; then
		    cdo mergetime LMDZGLAC_19500101_20051231_1M_fluxlat.nc LMDZGLAC_20060101_21001231_1M_fluxlat.nc tmp_fluxlat_1950_2100.nc
		    cdo setctomiss,1e20 tmp_fluxlat_1950_2100.nc tmp1_fluxlat_1950_2100.nc
		    cdo chname,fluxlat,hfls tmp1_fluxlat_1950_2100.nc hfls_monthly_IPSL_${exp}_1_195001-210012.nc
		elif [[ "${var}" == "hfss" ]]; then
		    cdo mergetime LMDZGLAC_19500101_20051231_1M_fluxsens.nc LMDZGLAC_20060101_21001231_1M_fluxsens.nc tmp_fluxsens_1950_2100.nc
		    cdo setctomiss,1e20 tmp_fluxsens_1950_2100.nc tmp1_fluxsens_1950_2100.nc
		    cdo chname,fluxsens,hfss tmp1_fluxsens_1950_2100.nc hfss_monthly_IPSL_${exp}_1_195001-210012.nc
		elif [[ "${var}" == "psl" ]]; then
		    if [[ $exp == CTL ]]; then
			cdo mergetime LMDZGLAC_19500101_20051231_1D_slp.nc LMDZGLAC_20060101_21001231_1D_slp.nc tmp_slp_1D_1950_2100.nc
			cdo monmean tmp_slp_1D_1950_2100.nc tmp_slp_1950_2100.nc
		    else
			cdo mergetime LMDZGLAC_19500101_20051231_1M_slp.nc LMDZGLAC_20060101_21001231_1M_slp.nc tmp_slp_1950_2100.nc
		    fi
		    cdo setctomiss,1e20 tmp_slp_1950_2100.nc tmp1_slp_1950_2100.nc
		    cdo chname,slp,psl tmp1_slp_1950_2100.nc psl_monthly_IPSL_${exp}_1_195001-210012.nc
		elif [[ "${var}" == "rlds" ]]; then
		    cdo mergetime LMDZGLAC_19500101_20051231_1M_LWdnSFC.nc LMDZGLAC_20060101_21001231_1M_LWdnSFC.nc tmp_LWdnSFC_1950_2100.nc
		    cdo setctomiss,1e20 tmp_LWdnSFC_1950_2100.nc tmp1_LWdnSFC_1950_2100.nc
		    cdo chname,LWdnSFC,rlds tmp1_LWdnSFC_1950_2100.nc rlds_monthly_IPSL_${exp}_1_195001-210012.nc
		elif [[ "${var}" == "rlus" ]]; then
		    cdo mergetime LMDZGLAC_19500101_20051231_1M_LWupSFCclr.nc LMDZGLAC_20060101_21001231_1M_LWupSFCclr.nc tmp_LWupSFCclr_1950_2100.nc
		    cdo setctomiss,1e20 tmp_LWupSFCclr_1950_2100.nc tmp1_LWupSFCclr_1950_2100.nc
		    cdo chname,LWupSFCclr,rlus tmp1_LWupSFCclr_1950_2100.nc rlus_monthly_IPSL_${exp}_1_195001-210012.nc
		elif [[ "${var}" == "rsds" ]]; then
		    cdo mergetime LMDZGLAC_19500101_20051231_1M_SWdnSFC.nc LMDZGLAC_20060101_21001231_1M_SWdnSFC.nc tmp_SWdnSFC_1950_2100.nc
		    cdo setctomiss,1e20 tmp_SWdnSFC_1950_2100.nc tmp1_SWdnSFC_1950_2100.nc
		    cdo chname,SWdnSFC,rsds tmp1_SWdnSFC_1950_2100.nc rsds_monthly_IPSL_${exp}_1_195001-210012.nc
		elif [[ "${var}" == "rsus" ]]; then
		    cdo mergetime LMDZGLAC_19500101_20051231_1M_SWupSFC.nc LMDZGLAC_20060101_21001231_1M_SWupSFC.nc tmp_SWupSFC_1950_2100.nc
		    cdo setctomiss,1e20 tmp_SWupSFC_1950_2100.nc tmp1_SWupSFC_1950_2100.nc
		    cdo chname,SWupSFC,rsus tmp1_SWupSFC_1950_2100.nc rsus_monthly_IPSL_${exp}_1_195001-210012.nc
		else
		    ncrcat -O LMDZGLAC_19500101_20051231_1M_${var}.nc LMDZGLAC_20060101_21001231_1M_${var}.nc ${var}_${timeperiod}_${model}_${exp}_1_195001-210012.nc
		fi
		ncrename -O -d time_counter,time -v time_counter,time ${var}_${timeperiod}_${model}_${exp}_1_195001-210012.nc
		rm tmp*
	    elif [[ "$timeperiod" == "daily" ]]; then
	    mv LMDZGLAC_19500101_20051231_1D_${var}.nc ${var}_${timeperiod}_IPSL_${exp}_19500101_20051231_1D.nc
	    mv LMDZGLAC_20060101_21001231_1D_${var}.nc ${var}_${timeperiod}_IPSL_${exp}_20060101_21001231_1D.nc
	    ncrcat -O ${var}_${timeperiod}_IPSL_${exp}_19500101_20051231_1D.nc ${var}_${timeperiod}_IPSL_${exp}_20060101_21001231_1D.nc ${var}_${timeperiod}_${model}_${exp}_1_195001-210012.nc    
	    fi


	fi

    done
done