#!/bin/bash
# File Name : ens_mean.sh
# Creation Date : 20/03/2014
# Last Modified : 
# Created By : Ruth Lorenz
# Purpose : prepare files and create ensemble mean for GLACE-CMIP5

###-------------------------------------------------------
###-------------------------------------------------------
module load netcdf
module load cdo
module load nco

##---------------------##
## user specifications ##
##-------------------- ##
exp=CTL
year_end=2100
timeperiod=monthly
masko=no #mask ocean?
 
for model in ACCESS CESM  EC-EARTH  ECHAM6 GFDL  IPSL
do
    if [[ "$model" == "GFDL" ]]; then
	year_start=1951
	elif
	   [[ "$model" == "CESM" ]] && [[ "$exp" == "CTL" ]]; then
	year_start=1955
	else
	year_start=1950
	fi

    dir=/srv/ccrc/data32/$USER/GLACE-CMIP5/$model/$exp/

##---------------------##
    cd ${dir}

    for var in tas tasmax tasmin hfls
    do
	if [[ "$model" == "GFDL" ]] ; then
	ncks -O -v ${var} ${var}_${timeperiod}_${model}_${exp}_1_${year_start}01-${year_end}12.nc ${var}_${timeperiod}_${model}_${exp}_1_${year_start}01-${year_end}12.nc
	fi
	ncatted -h -O -a _FillValue,,o,f,1.0e20 ${var}_${timeperiod}_${model}_${exp}_1_${year_start}01-${year_end}12.nc
	if [[ "$masko" == "yes" ]]; then
	    cdo gtc,0.9 ../sftlf_invariant_${model}.nc ../landfrac_gt09.nc
	    cdo setctomiss,0  ../landfrac_gt09.nc  ../landfrac_gt09_mask.nc
	    cdo mul ../landfrac_gt09_mask.nc ${var}_${timeperiod}_${model}_${exp}_1_${year_start}01-${year_end}12.nc ${var}_${timeperiod}_${model}_${exp}_1_${year_start}01-${year_end}12_masko.nc
	    cdo remapbil,r360x180 ${var}_${timeperiod}_${model}_${exp}_1_${year_start}01-${year_end}12_masko.nc ${var}_${timeperiod}_${model}_${exp}_1_${year_start}01-${year_end}12_remapbil1x1.nc
	else
	    cdo remapbil,r360x180 ${var}_${timeperiod}_${model}_${exp}_1_${year_start}01-${year_end}12.nc ${var}_${timeperiod}_${model}_${exp}_1_${year_start}01-${year_end}12_remapbil1x1.nc
	fi
	if [[ "$exp" != "CTL" ]]; then
	    if [[ "$model" != "GFDL" ]]; then
		let new_start=${year_start}+1
		ncks -O -d time,12, ${var}_${timeperiod}_${model}_${exp}_1_${year_start}01-${year_end}12_remapbil1x1.nc ${var}_${timeperiod}_${model}_${exp}_1_${new_start}01-${year_end}12_remapbil1x1.nc
		elif [[ "$model" == "GFDL" ]] ; then
		ncks -O -d time,0,1799 ${var}_${timeperiod}_${model}_${exp}_1_${year_start}01-${year_end}12_remapbil1x1.nc ${var}_${timeperiod}_${model}_${exp}_1_${new_start}01-${year_end}12_remapbil1x1.nc
		fi
	    else
	    if [[ "$model" != "GFDL" ]] && [[ "$model" != "CESM" ]] ; then
		let new_start=${year_start}+5
		ncks -O -d time,60, ${var}_${timeperiod}_${model}_${exp}_1_${year_start}01-${year_end}12_remapbil1x1.nc ${var}_${timeperiod}_${model}_${exp}_1_${new_start}01-${year_end}12_remapbil1x1.nc
		elif [[ "$model" == "GFDL" ]] ; then
		let new_start=${year_start}+4
		ncks -O -d time,48, ${var}_${timeperiod}_${model}_${exp}_1_${year_start}01-${year_end}12_remapbil1x1.nc ${var}_${timeperiod}_${model}_${exp}_1_${new_start}01-${year_end}12_remapbil1x1.nc
		fi
	fi

    done #var
done #model
    
if [[ "$exp" != "CTL" ]]; then
    year_start=1951
else
     year_start=1955
fi

indir=/srv/ccrc/data32/$USER/GLACE-CMIP5
outdir=/srv/ccrc/data32/$USER/GLACE-CMIP5/ENS_MEAN/$exp/
mkdir -p $outdir

for var in tas tasmax tasmin hfls
    do
	cdo ensmean $indir/ACCESS/$exp/${var}_${timeperiod}_ACCESS_${exp}_1_${year_start}01-${year_end}12_remapbil1x1.nc $indir/CESM/$exp/${var}_${timeperiod}_CESM_${exp}_1_${year_start}01-${year_end}12_remapbil1x1.nc $indir/EC-EARTH/$exp/${var}_${timeperiod}_EC-EARTH_${exp}_1_${year_start}01-${year_end}12_remapbil1x1.nc $indir/ECHAM6/$exp/${var}_${timeperiod}_ECHAM6_${exp}_1_${year_start}01-${year_end}12_remapbil1x1.nc $indir/GFDL/$exp/${var}_${timeperiod}_GFDL_${exp}_1_${year_start}01-${year_end}12_remapbil1x1.nc $indir/IPSL/$exp/${var}_${timeperiod}_IPSL_${exp}_1_${year_start}01-${year_end}12_remapbil1x1.nc $outdir/${var}_${timeperiod}_ENS_MEAN_${exp}_1_${year_start}01-${year_end}12.nc
done

#clean up

#END