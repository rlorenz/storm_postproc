#!/bin/bash
# File Name : ens_mean_coupling.sh
# Creation Date : 25/03/2014
# Last Modified : Fri 03 Oct 2014 17:00:59 EST
# Created By : Ruth Lorenz
# Purpose : calculate ensemble mean of coupling strength 

###-------------------------------------------------------

module load netcdf
module load cdo
module load nco

##---------------------##
## user specifications ##
##-------------------- ##
exp=GC1A85
year_start=1981
year_end=2000
outdir=/srv/ccrc/data32/z3441306/GLACE-CMIP5_plots/ENS_MEAN/$exp/coupling/
mkdir -p ${outdir}

for Var in T TX TN PR
#for Var in T PR
do

    for model in ACCESS CESM  EC-EARTH  ECHAM6 GFDL  IPSL
#    for model in CESM
    do

	indir=/srv/ccrc/data32/z3441306/GLACE-CMIP5_plots/$model/$exp/coupling/
	cd ${indir}

	cdo remapcon,/srv/ccrc/data32/$USER/GLACE-CMIP5/IPSL/sftlf_invariant_IPSL.nc GCCMIP5_omega_coupstr_${exp}_CTL_${Var}_${year_start}-${year_end}.nc ${outdir}/${model}_GCCMIP5_omega_coupstr_${exp}_CTL_${Var}_${year_start}-${year_end}_remapcon.nc

    done    


cd ${outdir}
cdo ensmean ACCESS_GCCMIP5_omega_coupstr_${exp}_CTL_${Var}_${year_start}-${year_end}_remapcon.nc CESM_GCCMIP5_omega_coupstr_${exp}_CTL_${Var}_${year_start}-${year_end}_remapcon.nc EC-EARTH_GCCMIP5_omega_coupstr_${exp}_CTL_${Var}_${year_start}-${year_end}_remapcon.nc ECHAM6_GCCMIP5_omega_coupstr_${exp}_CTL_${Var}_${year_start}-${year_end}_remapcon.nc GFDL_GCCMIP5_omega_coupstr_${exp}_CTL_${Var}_${year_start}-${year_end}_remapcon.nc IPSL_GCCMIP5_omega_coupstr_${exp}_CTL_${Var}_${year_start}-${year_end}_remapcon.nc ENS_MEAN_GCCMIP5_omega_coupstr_${exp}_CTL_${Var}_${year_start}-${year_end}_remapcon.nc

done
