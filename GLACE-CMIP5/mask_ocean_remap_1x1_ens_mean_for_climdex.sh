#!/bin/bash
# File Name : mask_ocean_remap_1x1_for_ens_mean_for_climdex.sh
# Creation Date : 04/04/2014
# Last Modified : 
# Created By : Ruth Lorenz
# Purpose : mask ocean on original grid of each model
#       regrid files from all GLACE-CMIP5 model onto
#	common grid, 1x1, for calculation of ensemble means	

###-------------------------------------------------------
module load netcdf
module load cdo
module load nco

##---------------------##
## user specifications ##
##-------------------- ##
exp=CTL
year_end=2100
for model in ACCESS EC-EARTH #CESM  EC-EARTH  ECHAM6 GFDL  IPSL
do
    if [[ "$model" == "GFDL" ]]; then
	year_start=1950
	elif
	   [[ "$model" == "CESM" ]] && [[ "$exp" == "CTL" ]]; then
	year_start=1954
	else
	year_start=1949
	fi

    dir=/srv/ccrc/data32/$USER/GLACE-CMIP5/$model/$exp/climdex_index/

##---------------------##
    cd ${dir}

    for index in CDD CSDI CWD DTR FD GSL ID PRCPTOT R10mm R20mm R95p R99p Rnnmm Rx1day Rx5day SDII SU TN10p TN50p TN90p TNn TNx TR TX10p TX50p TX90p TXn TXx WSDI
    do
	cdo div ${model}_${exp}_1_${year_start}-${year_end}_${index}.nc /srv/ccrc/data32/$USER/GLACE-CMIP5/$model/landfrac_gt09_mask.nc ${model}_${exp}_1_${year_start}-${year_end}_${index}_maskocean.nc
	cdo remapbil,r360x180 ${model}_${exp}_1_${year_start}-${year_end}_${index}_maskocean.nc ${model}_${exp}_1_${year_start}-${year_end}_${index}_maskocean_remapbil1x1.nc
	if [[ "$exp" != "CTL" ]]; then
	    if [[ "$model" != "GFDL" ]]; then
		ncks -O -d time,1, ${model}_${exp}_1_${year_start}-${year_end}_${index}_maskocean_remapbil1x1.nc ${model}_${exp}_1_1950-${year_end}_${index}_maskocean_remapbil1x1.nc
		fi
	    else
	    if [[ "$model" != "GFDL" ]] && [[ "$model" != "CESM" ]] ; then
		ncks -O -d time,5, ${model}_${exp}_1_${year_start}-${year_end}_${index}_maskocean_remapbil1x1.nc ${model}_${exp}_1_1954-${year_end}_${index}_maskocean_remapbil1x1.nc
		elif [[ "$model" == "GFDL" ]] ; then
		ncks -O -d time,4, ${model}_${exp}_1_${year_start}-${year_end}_${index}_maskocean_remapbil1x1.nc ${model}_${exp}_1_1954-${year_end}_${index}_maskocean_remapbil1x1.nc
		fi
    fi
	
    done

done #model

#calculate ensemble mean
if [[ "$exp" != "CTL" ]]; then
    year_start=1950
else
     year_start=1954
fi

indir=/srv/ccrc/data32/$USER/GLACE-CMIP5
outdir=/srv/ccrc/data32/$USER/GLACE-CMIP5/ENS_MEAN/$exp/climdex_index
mkdir -p $outdir

for index in CDD CSDI CWD DTR FD GSL ID PRCPTOT R10mm R20mm R95p R99p Rnnmm Rx1day Rx5day SDII SU TN10p TN50p TN90p TNn TNx TR TX10p TX50p TX90p TXn TXx WSDI
do
cdo ensmean $indir/ACCESS/$exp/climdex_index/ACCESS_${exp}_1_${year_start}-${year_end}_${index}_maskocean_remapbil1x1.nc $indir/CESM/$exp/climdex_index/CESM_${exp}_1_${year_start}-${year_end}_${index}_maskocean_remapbil1x1.nc $indir/EC-EARTH/$exp/climdex_index/EC-EARTH_${exp}_1_${year_start}-${year_end}_${index}_maskocean_remapbil1x1.nc $indir/ECHAM6/$exp/climdex_index/ECHAM6_${exp}_1_${year_start}-${year_end}_${index}_maskocean_remapbil1x1.nc $indir/GFDL/$exp/climdex_index/GFDL_${exp}_1_${year_start}-${year_end}_${index}_maskocean_remapbil1x1.nc $indir/IPSL/$exp/climdex_index/IPSL_${exp}_1_${year_start}-${year_end}_${index}_maskocean_remapbil1x1.nc $outdir/ENS_MEAN_${exp}_1_${year_start}-${year_end}_${index}_maskocean.nc
done #index


#clean up

#END