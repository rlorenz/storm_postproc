#!/bin/bash
# File Name : mask_ocean_remap_coarsest.sh
# Creation Date : 04/04/2014
# Last Modified : Mon 12 Jan 2015 16:52:41 AEDT
# Created By : Ruth Lorenz
# Purpose : mask ocean for each model on own land masssk
#           remap GLACE-CMIP5 files to coarsest grid among them        	

###-------------------------------------------------------
module load netcdf
module load cdo
module load nco

##---------------------##
## user specifications ##
##-------------------- ##
exp=GC1A85
timeperiod=monthly
year_end=210012
#for model in ACCESS EC-EARTH CESM  EC-EARTH  ECHAM6 GFDL IPSL
for model in ECHAM6
do
    if [[ "$model" == "GFDL" ]]; then
	year_start=195101
	elif
	   [[ "$model" == "CESM" ]] && [[ "$exp" == "CTL" ]]; then
	year_start=195501
	else
	year_start=195001
	fi

    dir=/srv/ccrc/data32/$USER/GLACE-CMIP5/$model/$exp/

##---------------------##
    cd ${dir}

    #for variable in hfls tas mrso mrsos
    for variable in mrso
    do


	if [[ "$exp" != "CTL" ]]; then
	    if [[ "$model" != "GFDL" ]] && [[ "$model" != "IPSL" ]] ; then
		ncks -O -d time,12, ${variable}_${timeperiod}_${model}_${exp}_1_${year_start}-${year_end}.nc ${variable}_${timeperiod}_${model}_${exp}_1_195101-${year_end}.nc
		if [[ "$var" != "mrsos" ]] && [[ "$var" != "mrso" ]] ; then
		cdo div ${variable}_${timeperiod}_${model}_${exp}_1_195101-${year_end}.nc /srv/ccrc/data32/$USER/GLACE-CMIP5/${model}/landfrac_gt09_mask.nc ${variable}_${timeperiod}_${model}_${exp}_1_195101-${year_end}_maskocean.nc
		else
		    cp ${variable}_${timeperiod}_${model}_${exp}_1_195101-${year_end}.nc ${variable}_${timeperiod}_${model}_${exp}_1_195101-${year_end}_maskocean.nc
		fi
	    cdo remapcon,/srv/ccrc/data32/$USER/GLACE-CMIP5/IPSL/sftlf_invariant_IPSL.nc ${variable}_${timeperiod}_${model}_${exp}_1_195101-${year_end}_maskocean.nc ${variable}_${timeperiod}_${model}_${exp}_1_195101-${year_end}_maskocean_remapcon.nc
	    elif [[ "$model" == "GFDL" ]]; then
		ncks -O -v ${variable} ${variable}_${timeperiod}_${model}_${exp}_1_195101-${year_end}.nc ${variable}_${timeperiod}_${model}_${exp}_1_195101-${year_end}_tmp.nc
		if [[ "$variable" != "mrsos" ]] && [[ "$variable" != "mrso" ]] ; then
		cdo div ${variable}_${timeperiod}_${model}_${exp}_1_195101-${year_end}.nc /srv/ccrc/data32/$USER/GLACE-CMIP5/${model}/landfrac_gt09_mask.nc ${variable}_${timeperiod}_${model}_${exp}_1_195101-${year_end}_maskocean.nc
		else
		    cp ${variable}_${timeperiod}_${model}_${exp}_1_195101-${year_end}.nc ${variable}_${timeperiod}_${model}_${exp}_1_195101-${year_end}_maskocean.nc
		fi	
		cdo remapcon,/srv/ccrc/data32/$USER/GLACE-CMIP5/IPSL/sftlf_invariant_IPSL.nc ${variable}_${timeperiod}_${model}_${exp}_1_195101-${year_end}_maskocean.nc ${variable}_${timeperiod}_${model}_${exp}_1_195101-${year_end}_maskocean_remapcon.nc
		rm ${variable}_${timeperiod}_${model}_${exp}_1_195101-${year_end}_tmp.nc
	    elif [[ "$model" == "IPSL" ]]; then
		ncks -O -d time,12, ${variable}_${timeperiod}_${model}_${exp}_1_${year_start}-${year_end}.nc ${variable}_${timeperiod}_${model}_${exp}_1_195101-${year_end}.nc
		if [[ "$variable" != "mrsos" ]] && [[ "$variable" != "mrso" ]] ; then
		cdo div ${variable}_${timeperiod}_${model}_${exp}_1_195101-${year_end}.nc /srv/ccrc/data32/$USER/GLACE-CMIP5/${model}/landfrac_gt09_mask.nc ${variable}_${timeperiod}_${model}_${exp}_1_195101-${year_end}_maskocean.nc
		else
		    cp ${variable}_${timeperiod}_${model}_${exp}_1_195101-${year_end}.nc ${variable}_${timeperiod}_${model}_${exp}_1_195101-${year_end}_maskocean.nc
		fi
		cp ${variable}_${timeperiod}_${model}_${exp}_1_195101-${year_end}_maskocean.nc ${variable}_${timeperiod}_${model}_${exp}_1_195101-${year_end}_maskocean_remapcon.nc
	    fi

	else
	    if [[ "$model" != "GFDL" ]] && [[ "$model" != "CESM" ]] ; then
		ncks -O -d time,60, ${variable}_${timeperiod}_${model}_${exp}_1_${year_start}-${year_end}.nc ${variable}_${timeperiod}_${model}_${exp}_1_195501-${year_end}.nc
	    elif [[ "$model" == "GFDL" ]] ; then
		ncks -O -d time,48, ${variable}_${timeperiod}_${model}_${exp}_1_${year_start}-${year_end}.nc ${variable}_${timeperiod}_${model}_${exp}_1_195501-${year_end}.nc
		ncks -O -v ${variable} ${variable}_${timeperiod}_${model}_${exp}_1_195501-${year_end}.nc ${variable}_${timeperiod}_${model}_${exp}_1_195501-${year_end}.nc
	    fi
	    if [[ "$variable" != "mrsos" ]] && [[ "$variable" != "mrso" ]] ; then
		cdo div ${variable}_${timeperiod}_${model}_${exp}_1_195501-${year_end}.nc /srv/ccrc/data32/$USER/GLACE-CMIP5/${model}/landfrac_gt09_mask.nc ${variable}_${timeperiod}_${model}_${exp}_1_195501-${year_end}_maskocean.nc
	    else
		cp ${variable}_${timeperiod}_${model}_${exp}_1_195501-${year_end}.nc ${variable}_${timeperiod}_${model}_${exp}_1_195501-${year_end}_maskocean.nc
	    fi
	    if [[ "$model" != "IPSL" ]] ; then
		cdo remapcon,/srv/ccrc/data32/$USER/GLACE-CMIP5/IPSL/sftlf_invariant_IPSL.nc ${variable}_${timeperiod}_${model}_${exp}_1_195501-${year_end}_maskocean.nc ${variable}_${timeperiod}_${model}_${exp}_1_195501-${year_end}_maskocean_remapcon.nc
	    else
		cp ${variable}_${timeperiod}_${model}_${exp}_1_195501-${year_end}_maskocean.nc ${variable}_${timeperiod}_${model}_${exp}_1_195501-${year_end}_maskocean_remapcon.nc
	    fi
	 fi
    done

done #model

#clean up

#END
