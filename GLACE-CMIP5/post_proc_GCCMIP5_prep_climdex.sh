#!/bin/bash
# File Name : post_proc_GCCMIP5_prep_climdex.sh
# Creation Date : 2014/01/06
# Last Modified : Mon 06 Jan 2014 12:45:51 EST
# Created By : Ruth Lorenz
# Purpose : prepare/format GLACE-CMIP5 output files for
#	 	fclimdex, remove missing_value attribute 
#		and set reference time to 1950-01-01 to
#		avoid negative time values

###------------------------------------------------------
module load netcdf
module load nco
module load cdo

##---------------------##
## user specifications ##
##-------------------- ##
model=ACCESS
exp=CTL
year_start=195001
year_end=210012
dir=/srv/ccrc/data32/$USER/GLACE-CMIP5/$model/$exp/
#dir=/srv/ccrc/data23/$USER/GLACE-CMIP5/CESM/$exp/

##---------------------##
mkdir ${dir}
mkdir ${dir}/climdex_index/

cd ${dir}

for var in pr tasmax tasmin tas
do
      cdo setreftime,1950-01-01,00:00:00,days ${var}_daily_${model}_${exp}_1_${year_start}-${year_end}.nc ${var}_daily_${model}_${exp}_1_${year_start}-${year_end}_ref1950.nc
      ncatted -h -O -a _FillValue,,o,f,1.0e20 ${var}_daily_${model}_${exp}_1_${year_start}-${year_end}_ref1950.nc
      ncatted -h -O -a _FillValue,time,d,, ${var}_daily_${model}_${exp}_1_${year_start}-${year_end}_ref1950.nc
      ncatted -h -O -a missing_value,${var},d,, ${var}_daily_${model}_${exp}_1_${year_start}-${year_end}_ref1950.nc ${var}_daily_${model}_${exp}_1_${year_start}-${year_end}.nc

done

#clean up
rm *_ref1950.nc

#END
