#!/bin/bash
# File Name : remapcon_ens_mean_for_climdex.sh
# Creation Date : 05/03/2014
# Last Modified : 
# Created By : Ruth Lorenz
# Purpose : regrid files from all GLACE-CMIP5 model onto
#	common grid, for calculation of ensemble means	

###-------------------------------------------------------
module load netcdf
module load cdo
module load nco

##---------------------##
## user specifications ##
##-------------------- ##
exp=GC1B85
year_end=2100
#for model in ACCESS CESM  EC-EARTH ECHAM6 GFDL  IPSL
for model in ACCESS EC-EARTH
do
    if [[ "$model" == "GFDL" ]]; then
	year_start=1950
	elif
	   [[ "$model" == "CESM" ]] && [[ "$exp" == "CTL" ]]; then
	year_start=1954
	else
	year_start=1949
	fi

    dir=/srv/ccrc/data32/$USER/GLACE-CMIP5/$model/$exp/climdex_index/

##---------------------##
    cd ${dir}

    for index in CDD CSDI CWD DTR FD GSL ID PRCPTOT R10mm R20mm R95p R99p Rnnmm Rx1day Rx5day SDII SU TN10p TN50p TN90p TNn TNx TR TX10p TX50p TX90p TXn TXx WSDI
    do
	cdo remapcon,/srv/ccrc/data32/$USER/GLACE-CMIP5/IPSL/sftlf_invariant_IPSL.nc ${model}_${exp}_1_${year_start}-${year_end}_${index}.nc ${model}_${exp}_1_${year_start}-${year_end}_${index}_remapcon.nc
	cdo remapcon,/srv/ccrc/data32/$USER/GLACE-CMIP5/ACCESS/sftlf_invariant_ACCESS.nc ${model}_${exp}_1_${year_start}-${year_end}_${index}.nc ${model}_${exp}_1_${year_start}-${year_end}_${index}_remapconn96.nc
	if [[ "$exp" != "CTL" ]]; then
	    if [[ "$model" != "GFDL" ]]; then
		ncks -O -d time,1, ${model}_${exp}_1_${year_start}-${year_end}_${index}_remapcon.nc ${model}_${exp}_1_1950-${year_end}_${index}_remapcon.nc
		ncks -O -d time,1, ${model}_${exp}_1_${year_start}-${year_end}_${index}_remapconn96.nc ${model}_${exp}_1_1950-${year_end}_${index}_remapconn96.nc
		fi
	    else
	    if [[ "$model" != "GFDL" ]] && [[ "$model" != "CESM" ]] ; then
		ncks -O -d time,5, ${model}_${exp}_1_${year_start}-${year_end}_${index}_remapcon.nc ${model}_${exp}_1_1954-${year_end}_${index}_remapcon.nc
		ncks -O -d time,5, ${model}_${exp}_1_${year_start}-${year_end}_${index}_remapconn96.nc ${model}_${exp}_1_1954-${year_end}_${index}_remapconn96.nc
		elif [[ "$model" == "GFDL" ]] ; then
		ncks -O -d time,4, ${model}_${exp}_1_${year_start}-${year_end}_${index}_remapcon.nc ${model}_${exp}_1_1954-${year_end}_${index}_remapcon.nc
		ncks -O -d time,4, ${model}_${exp}_1_${year_start}-${year_end}_${index}_remapconn96.nc ${model}_${exp}_1_1954-${year_end}_${index}_remapconn96.nc
		fi
    fi
	
    done

done #model

calculate ensemble mean
if [[ "$exp" != "CTL" ]]; then
    year_start=1950
else
     year_start=1954
fi

indir=/srv/ccrc/data32/$USER/GLACE-CMIP5
outdir=/srv/ccrc/data32/$USER/GLACE-CMIP5/ENS_MEAN/$exp/climdex_index
mkdir -p $outdir

for index in CDD CSDI CWD DTR FD GSL ID PRCPTOT R10mm R20mm R95p R99p Rnnmm Rx1day Rx5day SDII SU TN10p TN50p TN90p TNn TNx TR TX10p TX50p TX90p TXn TXx WSDI
do
cdo ensmean $indir/ACCESS/$exp/climdex_index/ACCESS_${exp}_1_${year_start}-${year_end}_${index}_remapcon.nc $indir/CESM/$exp/climdex_index/CESM_${exp}_1_${year_start}-${year_end}_${index}_remapcon.nc $indir/EC-EARTH/$exp/climdex_index/EC-EARTH_${exp}_1_${year_start}-${year_end}_${index}_remapcon.nc $indir/ECHAM6/$exp/climdex_index/ECHAM6_${exp}_1_${year_start}-${year_end}_${index}_remapcon.nc $indir/GFDL/$exp/climdex_index/GFDL_${exp}_1_${year_start}-${year_end}_${index}_remapcon.nc $indir/IPSL/$exp/climdex_index/IPSL_${exp}_1_${year_start}-${year_end}_${index}_remapcon.nc $outdir/ENS_MEAN_${exp}_1_${year_start}-${year_end}_${index}.nc
done #index


#clean up

#END