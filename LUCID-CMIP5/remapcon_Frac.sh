#!/bin/bash
# File Name : remapcon_ens_mean_for_climdex.sh
# Creation Date : 05/03/2014
# Last Modified : Mon 29 Jun 2015 18:29:56 AEST
# Created By : Ruth Lorenz
# Purpose : regrid files from all LUCID-CMIP5 model onto
#	common grid, for calculation of ensemble means	

###-------------------------------------------------------
module load netcdf
module load cdo
module load nco

##---------------------##
## user specifications ##
##-------------------- ##
exp=rcp26
ens=r1i1p1

#for model in CanESM2 HadGEM2-ES IPSL-CM5A-LR MIROC-ESM MPI-ESM-LR
for model in HadGEM2-ES IPSL-CM5A-LR MIROC-ESM MPI-ESM-LR
do
	if [[ ${model} == HadGEM2-ES ]]; then
	    freq=Lmon
	    year_start=200601
	    year_end=209912
	elif [[ ${model} == CanESM2 ]]; then
	    freq=yr
	    year_start=2005
	    year_end=2100
	elif [[ ${model} == IPSL-CM5A-LR ]]; then
	    freq=Lmon
	    year_start=200601
	    year_end=230012
	else
	    freq=Lmon
	    year_start=200601
	    year_end=210012
	fi

    dir=/srv/ccrc/data44/$USER/LUCID-CMIP5/$model/$exp/

    cd ${dir}
    cdo remapcon2,r360x180 treeFrac_${freq}_${model}_${exp}_${ens}_${year_start}-${year_end}.nc treeFrac_${model}_${exp}_${ens}_${year_start}-${year_end}_remapcon2_1x1.nc

done #model

#clean up

#END
