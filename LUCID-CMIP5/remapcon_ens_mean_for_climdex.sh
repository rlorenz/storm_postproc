#!/bin/bash
# File Name : remapcon_ens_mean_for_climdex.sh
# Creation Date : 05/03/2014
# Last Modified : mer. 03 juin 2015 19:34:56 AEST
# Created By : Ruth Lorenz
# Purpose : regrid files from all LUCID-CMIP5 model onto
#	common grid, for calculation of ensemble means	

###-------------------------------------------------------
module load netcdf
module load cdo
module load nco

##---------------------##
## user specifications ##
##-------------------- ##
exp=L2A26
ens=r1i1p1
year_start=2005
year_end=2100
for model in CanESM2 HadGEM2-ES IPSL-CM5A-LR MIROC-ESM MPI-ESM-LR
#for model in MIROC-ESM MPI-ESM-LR
do
	if [[ ${model} == HadGEM2-ES ]] && [[ ${exp} == L2A26 ]]; then
		year_end=2099
	else
		year_end=2100
	fi

    dir=/srv/ccrc/data44/$USER/LUCID-CMIP5/$model/$exp/climdex_index/

##---------------------##
    cd ${dir}

    for index in CDD CSDI CWD DTR FD GSL ID PRCPTOT R10mm R20mm R95p R99p Rnnmm Rx1day Rx5day SDII SU TN10p TN50p TN90p TNn TNx TR TX10p TX50p TX90p TXn TXx WSDI
    do
	cdo remapcon2,r360x180 ${model}_${exp}_${ens}_${year_start}-${year_end}_${index}.nc ${model}_${exp}_${ens}_${year_start}-${year_end}_${index}_remapcon2_1x1.nc
	
    done

done #model

#clean up

#END
